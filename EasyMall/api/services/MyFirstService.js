// File location: /api/services/MyFirstService.js


var MyFirstService = {
  sayHello: function sayHelloService() {
    return 'Hello I am the real Service';
  },


  /*getMalls: function (opts,cb){
    var malls = Mall.find().exec(function (err,malls) {
      if (err) return cb(err);
      cb(null,malls);
    });
  },*/
  getMalls: function getMalls(){
    //get data from model
    sails.log("start service");
    Mall.find().exec(function(err,malls){
      if(err){
        sails.log(err);
        return err;
      }
      return malls;
    });
  },

  /*createMall: function createUserService(req,res){
	  var mallParams = req.params.all()

	  Mall.create(
	  {
		  name: mallName, address: mallAddress, openHour: mallOpenHour, closeHour: mallCloseHour
	  }.exec(function createCB(err, created){
		  console.log('created mall with name ' + created.name);
	  }))
  }
  return Mall.*/
};


module.exports = MyFirstService;
