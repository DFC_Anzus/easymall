module.exports = {


  attributes: {
    name : {
        type : 'string',
        required: true
    },
    description : {
        type : 'string',
        required: true
    },
	  date: {
		  type: 'date',
      required: true
    },
	
	mall: {
		model: 'mall'
	}
  }
};

