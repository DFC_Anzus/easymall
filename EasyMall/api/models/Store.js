module.exports = {


  attributes: {
    name: {
      type : 'string',
      required: true
    },
    locationNumber: {
      type : 'string',
      required: true
    },
    suscription: {
      type : 'integer',
      defaultsTo: 0
    },
    openHour: {
      type : 'integer',
      defaultsTo: 9
    },
    closeHour: {
      type : 'integer',
      defaultsTo: 10
    },


    owner: {
      model: 'client'
    },

    mall: {
      model: 'mall'
    },

    products: {
      collection: 'product',
      via: 'branch'
    },

    offers: {
      collection: 'offer',
      via: 'branch'
    }

  }
};

