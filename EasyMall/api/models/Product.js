module.exports = {


  attributes: {
	name: {
        type : 'string',
		required: true
    },
    description: {
        type : 'string',
		required: false
    },
    value: {
        type : 'string',
		required: false
    },

	branch: {
		model: 'Store'
	},

	category: {
		model: 'Category'
	}

  }
};

