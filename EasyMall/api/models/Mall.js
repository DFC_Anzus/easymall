module.exports = {


  attributes: {
    name: {
        type : 'string',
		unique: true,
		required: true
    },
    address: {
        type : 'string',
		required: true
    },
	openHour: {
        type : 'integer',
        defaultsTo: 9
    },
	closeHour: {
        type : 'integer',
        defaultsTo: 10
    },


	stores:{
		collection: 'store',
		via: 'mall'

	},

    events:{
	    collection: 'event',
      via: 'mall'
    },

    parkings:{
      collection: 'parking',
      via: 'mall'
    }

  }
};

