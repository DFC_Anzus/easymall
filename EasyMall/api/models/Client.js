module.exports = {

  
  attributes: {
    name: {
        type : 'string',
		unique: true,
		required: true
    },
    phone: {
        type : 'string',
		required: true
    },
	email: {
        type : 'integer'
    },
	
	branches:{
		collection: 'store',
		via: 'owner'
		
	}
	
  }
};

