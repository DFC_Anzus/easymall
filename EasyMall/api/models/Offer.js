module.exports = {


  attributes: {
    description : {
        type : 'string',
        required: true
    },
	  startTime: {
		  type: 'date',
      required: true
    },
    endTime: {
      type: 'date',
      required: true
    },
	
	branch: {
		model: 'Store'
	}
  }
};

