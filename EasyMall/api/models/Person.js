module.exports = {


  attributes: {
    name : {
        type : 'string',
        required: true
    },
    age : {
        type : 'int',
        required: true
    },
	  email: {
		  type: 'string',
      required: true
    },
    inMall: {
      type: 'boolean',
      required: true
    },
    location: {
      type: 'string',
      required: true
    },

	favorites: {
		collection: 'favorite',
		via: 'person'
	}
  }
};

