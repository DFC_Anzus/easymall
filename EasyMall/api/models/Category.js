module.exports = {


  attributes: {
    name : {
        type : 'string'
    },
    description : {
        type : 'string'
    },
	  products: {
		  collection: 'product',
		  via: 'category'
    }
  }
};

