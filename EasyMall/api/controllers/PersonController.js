/**
 * PersonController
 *
 * @description :: Server-side logic for managing people
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  //http://localhost:1337/person/getImage?id=1
  getImage: function(req, res){
        Person.findOne(req.param('id')).exec(function (err, user){
        if (err) return res.negotiate(err);
        if (!user) return res.notFound();

        // User has no avatar image uploaded.

        var SkipperDisk = require('skipper-disk');
        var fileAdapter = SkipperDisk(/* optional opts */);
        // Stream the file down
        fileAdapter.read('./assets/images/Photo_1.jpg')
          .on('error', function (err){
            return res.serverError(err);
          })
          .pipe(res);
      });

  }
}

