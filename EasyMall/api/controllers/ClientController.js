/**
 * ClientController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  //usage: Client/getStores/:id
  getStores: function(req, res) {
    sails.log("function started");
    sails.log("looking for client")
    var client = null;
    //sails.log(req.params.id);
    Client.find({id:req.params.id}).exec(function(err,client){
      //sails.log(client.length);
      if(err || client.length == 0){
        return res.json({client: 'not user found'});
      }

      sails.log("client: "+ client[0].name);

      Store.find({owner: client[0].id}).exec(function(err,stores){
        //sails.log(stores);
        if(err){
          return res.negotiate(err);
        }
        return res.json(stores);
      });
    });



},
  //usage: Client/getActiveStores/:id
  getActiveStores: function(req, res){
    Client.find({id:req.params.id}).exec(function(err,client){
      //sails.log(client.length);
      if(err || client.length == 0){
        return res.json({client: 'not user found'});
      }

      //sails.log("client: "+ client[0].name);

      Store.find({owner: client[0].id, suscription: 1}).exec(function(err,stores){
        //sails.log(stores);
        if(err){
          return res.negotiate(err);
        }
        return res.json(stores);
      });
    });


  },


  //usage: Client/getBill/:id
  getBill: function(req, res){
    Client.find({id:req.params.id}).exec(function(err,client){
      //sails.log(client.length);
      if(err || client.length == 0){
        return res.json({client: 'not user found'});
      }

      //sails.log("client: "+ client[0].name);

      Store.find({owner: client[0].id}).exec(function(err,stores){
        var cost = 0;

        sails.log(stores);
        if(err){
          return res.negotiate(err);
        }
          stores.forEach(function(store){
            if(store.suscription == 0 || store.suscription == 2){
              cost=cost+680000;
            }
          });


          return res.json({Cost: cost});


      });
    });


  },


  //usage: Client/addStore?{"id" : "1", "name" : "A", "locationNumber" : "1", "mall" : "1"}
  addStore: function(req, res){
    Client.find({id:req.body.id}).exec(function(err,client){
      if(err || client.length == 0){
        return res.negotiate(err);
      }

      Store.find(
        {mall: req.body.mall,
          locationNumber:req.body.locationNumber}
          ).exec(function(err,store){
         if(err){
           return res.negotiate(err);
         }
         sails.log(store[0] + " " + store.length);
         if(store.length != 0){
           return res.json({locationNumber: "location Number Busy"});
         }


        Store.create({
          name:req.body.name,
          locationNumber:req.body.locationNumber,
          suscription:0,
          mall:req.body.mall,
          owner:req.body.id
        }).exec(function createCB(err, created){
          if(err){
            return res.negotiate(err);
          }
          return res.json(created);
        });


      });



    });


  },


  PaySuscription: function(req, res){
    Client.find({id:req.body.clientId}).exec(function(err,client){
      if(err || client.length == 0){
        return res.json({client: 'user not found'});
      }
      Store.find({id:req.body.storeId}).exec(function(err,store){
        sails.log(store[0].owner + " -> " + client[0].id);
        if(err || store.length == 0 || store[0].owner != client[0].id){
          return res.json({Store: 'Store not found'});
        }
        Store.update(
          {id:req.body.storeId},
          {suscription: 1}
          ).exec(function afterwards(err, updated){
          if(err){
            return res.negotiate(err);
          }
          return res.json(updated);
        })


      });
    });


  },


};

