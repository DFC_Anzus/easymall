/**
 * MallController
 *
 * @description :: Server-side logic for managing malls
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	HelloWorld: function (req, res){
        //return res.json({MyService: 'Jarts doesn´t exist'});

		var helloMessage = MyFirstService.sayHello();

		//res.send('our service has a message for you ' + helloMessage);
		return res.json({MyService: 'our service has a message for you ' + helloMessage});
    },


  getMalls: function(req, res) {
    sails.log.debug("Start Controller");
    var malls = MyFirstService.getMalls();
    sails.log(malls);
    return res.json(malls);
  },

  getStores: function(req, res) {
    Store.find({mall: req.params.id}).exec(function (err, stores) {
      sails.log(stores);
      if (err || stores.length == 0) {
        return res.json({client: 'Mall not found'});
      }
      return res.json({stores: stores});

    });

  }




};


